﻿using gpntb.Data;
using gpntb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;



       



namespace gpntb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(ILogger<HomeController> logger, UserManager<IdentityUser> userManager) 
        {
            _logger = logger;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var phoneNumber = user != null ? await _userManager.GetPhoneNumberAsync(user) : "Number nothing";

            ViewData["PhoneNumber"] = phoneNumber;
            if (User.Identity.IsAuthenticated)
            {
                using (IWebDriver driver = new ChromeDriver())
                {

                    try
                    {
                        driver.Navigate().GoToUrl($"http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID={phoneNumber}");
                        IWebElement dataElement = driver.FindElement(By.CssSelector(".advanced tr td"));
                        string dataText = dataElement.GetAttribute("innerHTML").Trim();
                        string[] fullName = dataText.Split(new string[] { "<br>" }, StringSplitOptions.None);
                        if (fullName.Length >= 3)
                        {
                            string last_name = fullName[0].Trim();
                            string first_name = fullName[1].Trim();
                            string middle_name = fullName[2].Trim();

                            ViewData["last_name"] = last_name;
                            ViewData["first_name"] = first_name;
                            ViewData["middle_name"] = middle_name;
                        }
                    }
                    finally
                    {
                        driver.Quit();
                    }

                }
            }
            return View();
        }

        [Authorize]
        public class ProfileController : Controller
        {
            private readonly ApplicationDbContext _context;

            public ProfileController(ApplicationDbContext context)
            {
                _context = context;
            }

        }

        public IActionResult Privacy()
        {

            using (IWebDriver driver = new ChromeDriver())
            {
                
                try
                {
                    driver.Navigate().GoToUrl("http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID=0006491011036");
                    IWebElement dataElement = driver.FindElement(By.CssSelector(".advanced tr td"));
                    string dataText = dataElement.GetAttribute("innerHTML").Trim();
                    string[] fullName = dataText.Split(new string[] { "<br>" }, StringSplitOptions.None);
                    if (fullName.Length >= 3)
                    {
                        string last_name = fullName[0].Trim();
                        string first_name= fullName[1].Trim();
                        string middle_name = fullName[2].Trim();

                        ViewData["last_name"] = last_name;
                        ViewData["first_name"] = first_name;
                        ViewData["middle_name"] = middle_name;
                    }
                }
                finally
                {
                    driver.Quit();
                }

            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        static string GetValueByKey(string input, string startKey, string endKey)
        {
            int startPos = input.IndexOf(startKey) + startKey.Length;
            if (startPos < startKey.Length) return ""; // Если ключ не найден, вернуть пустую строку

            int endPos = string.IsNullOrEmpty(endKey) ? input.Length : input.IndexOf(endKey, startPos);
            if (endPos < 0) endPos = input.Length; // Если конечный ключ не найден, идём до конца строки

            string value = input.Substring(startPos, endPos - startPos).Trim();
            return value.Split(new char[] { ' ' }, 2)[0].Trim(); // Возвращаем только первое слово/код после ключа, если это необходимо
        }

        public async Task<IActionResult> Orders()
        {
            var user = await _userManager.GetUserAsync(User);
            var phoneNumber = user != null ? await _userManager.GetPhoneNumberAsync(user) : "Number nothing";

            ViewData["PhoneNumber"] = phoneNumber;
            if (User.Identity.IsAuthenticated)
            {
                using (IWebDriver driver = new ChromeDriver())
                {
                    try
                    {
                        driver.Navigate().GoToUrl($"http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID={phoneNumber}");
                        /*IWebElement retroOrderLinkElement = driver.FindElement(By.XPath("//a[contains(text(),'Заказ по РЕТРОФОНДУ')]"));
                        string retroOrderLink = retroOrderLinkElement.GetAttribute("href");
                        ViewData["retroOrderLink"] = retroOrderLink;*/


                        /*IWebElement myFormularLinkElement = driver.FindElement(By.XPath("//a[contains(text(),'Мой формуляр')]"));
                        string myFormularLink = myFormularLinkElement.GetAttribute("href");*/

                        IWebElement myOrdersLinkElement = driver.FindElement(By.XPath("//a[contains(text(),'Мои заказы')]"));
                        string myOrdersLink = myOrdersLinkElement.GetAttribute("href");
                        driver.Navigate().GoToUrl($"{myOrdersLink}");
                        IReadOnlyList<IWebElement> advancedElements = driver.FindElements(By.ClassName("advanced"));

                        if (advancedElements.Count >= 2)
                        {
                            var infoElement = advancedElements[1];

                            string infoText = infoElement.Text;
                            Console.WriteLine($"Место выдачи: {infoText}");
                            ViewData["infoText"] = infoText;

                            string databaseCatalog = GetValueByKey(infoText, "База данных каталога:", "Шифр хранения:");
                            string storageCode = GetValueByKey(infoText, "Шифр хранения:", "История тела [Текст] :");
                            string orderDate = GetValueByKey(infoText, "Дата заказа:", "Заказ забронирован");
                            string storageLocation = GetValueByKey(infoText, "Место хранения:", "Место выдачи:");
                            string issuePlace = GetValueByKey(infoText, "Место выдачи:", "");
                            bool isOrderBooked = infoText.Contains("Заказ забронирован");
                            string orderBooked = isOrderBooked ? "Да" : "Нет";

                            Console.WriteLine($"База данных каталога: {databaseCatalog}");
                            Console.WriteLine($"Шифр хранения: {storageCode}");
                            Console.WriteLine($"Дата заказа: {orderDate}");
                            Console.WriteLine($"Место хранения: {storageLocation}");
                            Console.WriteLine($"Место выдачи: {issuePlace}");
                            //Console.WriteLine($"Заказ забронирован: {(isOrderBooked ? "Да" : "Нет")}");
                            Console.WriteLine($"Заказ забронирован: {orderBooked}");


                            ViewData["databaseCatalog"] = databaseCatalog;
                            ViewData["storageCode"] = storageCode;
                            ViewData["orderDate"] = orderDate;
                            ViewData["orderBooked"] = orderBooked;
                            ViewData["storageLocation"] = storageLocation;
                            ViewData["issuePlace"] = issuePlace;

                        }
                        else
                        {
                            Console.WriteLine("Need elem 'advanced' on page.");
                        }
                    }
                    finally
                    {
                        driver.Quit();
                    }

                }
            }
            return View();
        }
    }
}
