using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using weblib.Models;

namespace weblib.Data
{
    public class EntryContext : DbContext
    {
        public EntryContext (DbContextOptions<EntryContext> options)
            : base(options)
        {
        }

        public DbSet<weblib.Models.Entry> Entry { get; set; } = default!;
    }
}
